<?php
Route::get('/calculadora/suma', function () {
    $opciones = array(
        'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
    );
    $parametros = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false
    , 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180
    , 'stream_context' => stream_context_create($opciones) );
    $url = "http://www.dneonline.com/calculator.asmx?WSDL";

    try{
        $client = new SoapClient($url,$parametros);
        dd($client->Add(['intA' => '10', 'intB' =>'6']));
    }
    catch(SoapFault $fault) {
        dd($fault);
    }

});
Route::get('/clima', 'SoapController@clima')->name('clima');
Route::post('/suma', 'SoapController@suma')->name('soap.suma');
Route::post('/resta', 'SoapController@resta')->name('soap.resta');
Route::post('/multiplicacion', 'SoapController@multiplicacion')->name('soap.multiplicacion');
Route::post('/division', 'SoapController@division')->name('soap.division');
Route::get('/calculadora', 'SoapController@calculadora')->name('calculadora');
