<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>WSDL</title>

</head>

<body>
    <div class="row justify-content-center">
        <div class="col-sm-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Suma</h5>
              <form method="POST" action="{{route('soap.suma')}}">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="num1">Número uno</label>
                  <input type="number" class="form-control" id="num1" name="num1">
                </div>
                <div class="form-group">
                  <label for="num2">Número 2</label>
                  <input type="number" class="form-control" id="num2" name="num2">
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Resta</h5>
              <form method="POST" action="{{route('soap.resta')}}">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="num1">Número uno</label>
                  <input type="number" class="form-control" id="num1" name="num1">
                </div>
                <div class="form-group">
                  <label for="num2">Número 2</label>
                  <input type="number" class="form-control" id="num2" name="num2">
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Multiplicación</h5>
                <form method="POST" action="{{route('soap.multiplicacion')}}">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label for="num1">Número uno</label>
                    <input type="number" class="form-control" id="num1" name="num1">
                  </div>
                  <div class="form-group">
                    <label for="num2">Número 2</label>
                    <input type="number" class="form-control" id="num2" name="num2">
                  </div>
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">División</h5>
                <form method="POST" action="{{route('soap.division')}}">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label for="num1">Número uno</label>
                    <input type="number" class="form-control" id="num1" name="num1">
                  </div>
                  <div class="form-group">
                    <label for="num2">Número 2</label>
                    <input type="number" class="form-control" id="num2" name="num2">
                  </div>
                  <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
              </div>
            </div>
          </div>
        <div class="col-sm-6">
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Resultado</h5>
                @if  (is_array($resultado) || is_object($resultado))                
                @foreach ($resultado as $res)
                <h1>Su resultado es: {{$res}}</h1>
                @endforeach
                @else
                <h1>Su resultado es: 0</h1>
                @endif

              </div>
            </div>
          </div>
      </div>

</body>
</html>
