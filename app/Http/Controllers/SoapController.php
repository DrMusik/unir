<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
class SoapController extends BaseSoapController
{
    private $service;
    public function calculadora(){
        $resultado = 0;
        return view('soap',compact('resultado'));
    }
    public function suma(Request $request){
        
        $opciones = array(
            'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
        );
        $parametros = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false
        , 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180
        , 'stream_context' => stream_context_create($opciones) );
        $url = "http://www.dneonline.com/calculator.asmx?WSDL";
    
        try{
            $resultados = new \SoapClient($url,$parametros);
            $resultado = $resultados->Add(['intA' => $request->num1, 'intB' =>$request->num2]);
            //return $resultado;
            return view('soap',compact('resultado'));
        }
        catch(\SoapFault $fault) {
            dd($fault);
        }
    }
    public function resta(Request $request){
        $opciones = array(
            'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
        );
        $parametros = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false
        , 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180
        , 'stream_context' => stream_context_create($opciones) );
        $url = "http://www.dneonline.com/calculator.asmx?WSDL";
    
        try{
            $resultado = new \SoapClient($url,$parametros);
            $resultado = $resultado->Subtract(['intA' => $request->num1, 'intB' =>$request->num2]);
            return view('soap',compact('resultado'));
            //return dd($client->Subtract(['intA' => $request->num1, 'intB' =>$request->num2]));
        }
        catch(\SoapFault $fault) {
            dd($fault);
        }
    }
    public function multiplicacion(Request $request){
        $opciones = array(
            'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
        );
        $parametros = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false
        , 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180
        , 'stream_context' => stream_context_create($opciones) );
        $url = "http://www.dneonline.com/calculator.asmx?WSDL";
    
        try{
            $resultado = new \SoapClient($url,$parametros);
            $resultado = $resultado->Multiply(['intA' => $request->num1, 'intB' =>$request->num2]);
            return view('soap',compact('resultado'));
            //return dd($client->Subtract(['intA' => $request->num1, 'intB' =>$request->num2]));
        }
        catch(\SoapFault $fault) {
            dd($fault);
        }
    }
    public function division(Request $request){
        $opciones = array(
            'ssl' => array('ciphers'=>'RC4-SHA', 'verify_peer'=>false, 'verify_peer_name'=>false)
        );
        $parametros = array ('encoding' => 'UTF-8', 'verifypeer' => false, 'verifyhost' => false
        , 'soap_version' => SOAP_1_2, 'trace' => 1, 'exceptions' => 1, "connection_timeout" => 180
        , 'stream_context' => stream_context_create($opciones) );
        $url = "http://www.dneonline.com/calculator.asmx?WSDL";
    
        try{
            $resultado = new \SoapClient($url,$parametros);
            $resultado = $resultado->Divide(['intA' => $request->num1, 'intB' =>$request->num2]);
            return view('soap',compact('resultado'));
            //return dd($client->Subtract(['intA' => $request->num1, 'intB' =>$request->num2]));
        }
        catch(\SoapFault $fault) {
            dd($fault);
        }
    }
}
